# noinspection PyUnreachableCode
import os

if __debug__ and os.environ.get('TIMED', False):
    import atexit
    import functools
    import time


    def _timed(func):
        _timed.timers[func] = {"T": 0, "N": 0}

        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            t = time.perf_counter()
            try:
                res = func(*args, **kwargs)
                return res
            finally:
                t = time.perf_counter() - t
                _timed.timers[func]["T"] += t
                _timed.timers[func]["N"] += 1

        return wrapped


    @atexit.register
    def _report():
        print("\n".join(
                "Time spent in {:>24}: {} sec over {} iterations".format(
                    func.__name__, T['T'], T['N']
                ) for func, T in _timed.timers.items()
            )
        )


    _timed.timers = dict()
else:
    def _timed(func):
        return func
