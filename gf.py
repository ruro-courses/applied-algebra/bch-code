"""Implementation of basic arithmetic in Galois Fields."""

import csv
import warnings
import timings

import numpy as np
import numpy.lib.stride_tricks as stricks

# region Constants
elem_type = np.int_
alpha = np.asarray(0b10, dtype=elem_type)
poly_one = np.asarray([1], dtype=elem_type)
poly_var = np.asarray([1, 0], dtype=elem_type)
_elem_info = np.iinfo(elem_type)
_primitives = None


# endregion


# region Internals
def _sanitize_inputs(*args):
    """Convert inputs to numpy arrays of appropriate dtype."""
    sanitized = []
    for arg in args:
        sanitized.append(np.atleast_1d(np.asarray(arg, dtype=elem_type)))
    return sanitized


def _sanitize_polynomials(*args):
    """Trim polynomials.

    Trim polynomials, so that the zeroth element
    represents the highest coefficient.
    """
    sanitized = []
    for arg in args:
        tr = np.trim_zeros(arg, trim='f')
        sanitized.append(tr if tr.size else np.asarray([0], dtype=elem_type))
    return sanitized


@timings._timed
def _mul(x, y):
    """Compute element-wise product of x and y."""
    x, y = _sanitize_inputs(x, y)
    x, y = x.copy(), y.copy()
    result = np.zeros(np.broadcast(x, y).shape, dtype=elem_type)
    while np.any(y):
        np.copyto(result, add(result, x), where=y % 2 != 0)
        x <<= 1
        y >>= 1
    return result


@timings._timed
def _mod(x, m):
    """Compute element-wise modulo of x with regards to m."""
    x, m = _sanitize_inputs(x, m)
    correction = np.zeros(np.broadcast(x, m).shape, dtype=elem_type)
    mbits = msb(m)
    shx = msb(x) - mbits
    while np.any(shx >= 0):
        correction[...] = 0
        np.left_shift(m, shx, out=correction,
                      where=(shx >= 0) & (x & 1 << (shx + mbits) != 0))
        x = add(correction, x)
        shx -= 1
    return x


@timings._timed
def _mod_pow(x, y, m):
    """Compute element-wise exponentiation of x to the power y modulo m."""
    x, y = _sanitize_inputs(x, y)
    y = y.copy()
    result = np.ones(np.broadcast(x, y).shape, dtype=elem_type)
    while np.any(y):
        np.copyto(result, _mod(_mul(result, x), m), where=y % 2 != 0)
        x = _mod(_mul(x, x), m)
        y >>= 1
    return result


# endregion


# region Primitives and Power matrices
def get_primitives():
    """Load known primitive elements.

    Load known primitive elements from disk or
    fall back to hardcoded values < 100.
    """
    global _primitives
    if _primitives is None:
        try:
            with open('primitives.csv', newline='') as f:
                _primitives = frozenset(int(p) for p in next(csv.reader(f)))
        except FileNotFoundError:
            warnings.warn("Couldn't load primitives.csv, "
                          "falling back to short list")
            _primitives = frozenset({7, 11, 13, 19, 25, 37, 41,
                                     47, 55, 59, 61, 67, 91, 97})
        return _primitives
    else:
        return _primitives


@timings._timed
def gen_pow_matrix(primpoly):
    """Compute the power matrix for given primitive element."""
    assert primpoly in get_primitives(), \
        "{} is not a known primitive element.".format(primpoly)
    q = msb(primpoly)
    powers = np.arange(1, 2 ** q, dtype=elem_type)
    powers = _mod_pow(alpha, powers, m=primpoly)
    logs = (1 + np.argsort(powers)).astype(elem_type)
    return np.stack([logs, powers], axis=-1)


# endregion


# region Galois Field arithmetic
@timings._timed
def msb(x):
    """Compute the element-wise position of the most significant bit."""
    x, = _sanitize_inputs(x)
    px = x > 0
    res = px.astype(elem_type)
    np.log2(x, out=res, casting='unsafe', where=px)
    return res


@timings._timed
def add(x, y, *args, **kwargs):
    """Compute element-wise sum of x and y."""
    return np.atleast_1d(np.bitwise_xor(x, y, *args, **kwargs))


@timings._timed
def prod(x, y, pm):
    """Compute element-wise product of x and y.

    Assumes that operations are performed in field with power matrix pm.
    (See gen_pow_matrix.)
    """
    x, y, pm = _sanitize_inputs(x, y, pm)
    assert pm.ndim == 2 and pm.shape[1] == 2, "pm is not a valid power matrix."
    p = np.zeros(np.broadcast(x, y).shape, dtype=elem_type)
    p += np.take(pm[:, 0], x - 1, mode='wrap')
    p += np.take(pm[:, 0], y - 1, mode='wrap')
    p -= 1
    np.take(pm[:, 1], p, out=p, mode='wrap')
    np.copyto(p, 0, where=(x == 0) | (y == 0))
    return p


@timings._timed
def divide(x, y, pm):
    """Compute element-wise quotient of x over y.

    Assumes that operations are performed in field with power matrix pm.
    (See gen_pow_matrix.)
    """
    x, y, pm = _sanitize_inputs(x, y, pm)
    assert pm.ndim == 2 and pm.shape[1] == 2, "pm is not a valid power matrix."
    assert np.all(y), "division by 0 error."
    p = np.zeros(np.broadcast(x, y).shape, dtype=elem_type)
    p += np.take(pm[:, 0], x - 1, mode='wrap')
    p -= np.take(pm[:, 0], y - 1, mode='wrap')
    p -= 1
    np.take(pm[:, 1], p, out=p, mode='wrap')
    np.copyto(p, 0, where=x == 0)
    return p


@timings._timed
def power(x, y, pm):
    """Compute element-wise exponentiation of x to the power y.

    Assumes that operations are performed in field with power matrix pm.
    (See gen_pow_matrix.)
    """
    x, y = _sanitize_inputs(x, y)
    assert pm.ndim == 2 and pm.shape[1] == 2, "pm is not a valid power matrix."
    p = np.zeros(np.broadcast(x, y).shape, dtype=elem_type)
    p += np.take(pm[:, 0], x - 1, mode='wrap')
    p *= y
    p -= 1
    np.take(pm[:, 1], p, out=p, mode='wrap')
    np.copyto(p, 0, where=x == 0)
    return p


# noinspection PyShadowingBuiltins
@timings._timed
def sum(x, axis=0, **kwargs):
    """Compute the reduction sum of x along specified axis."""
    x, = _sanitize_inputs(x)
    return np.bitwise_xor.reduce(x, axis=axis, dtype=elem_type, **kwargs)


# endregion


# region Matrix Linear Algebra
@timings._timed
def inv(a, pm, other=None):
    """Compute the inverse of matrix a.

    Assumes that operations are performed in field with power matrix pm.
    (See gen_pow_matrix.)
    If other is not None, computes the equivalent of a^-1 @ other.
    If the inverse doesn't exist, returns np.nan
    """
    a, pm = _sanitize_inputs(a, pm)
    assert pm.ndim == 2 and pm.shape[1] == 2, "pm is not a valid power matrix."
    assert a.ndim == 2, 'a should be a ndarray matrix'
    assert a.shape[0] == a.shape[1], 'a should be a square matrix'
    d = a.shape[0]
    if other is None:
        other = np.eye(d, dtype=elem_type)
    else:
        other, = _sanitize_inputs(other)
    ai = np.concatenate([a, other], axis=-1)
    # Forward pass
    for i in range(d):
        if not ai[i, i]:
            j, = np.nonzero(ai[i + 1:, i])
            if not len(j):
                return np.nan
            else:
                j = 1 + i + j[0]
                ai[[i, j]] = ai[[j, i]]
        ai[i] = divide(ai[i], ai[i, i], pm)
        ai[i + 1:] = add(ai[i + 1:], prod(ai[i], ai[i + 1:, i:i + 1], pm))
    # Backward pass
    for i in range(d, 0, -1):
        ai[:i - 1] = add(ai[:i - 1], prod(ai[i - 1], ai[:i - 1, i - 1:i], pm))
    assert np.all(np.eye(d) == ai[:, :d]), \
        "Input matrix wasn't I after inversion"
    return np.squeeze(ai[:, d:])


@timings._timed
def linsolve(a, b, pm):
    """Solve the system of linear equations a @ x = b.

    Assumes that operations are performed in field with power matrix pm.
    (See gen_pow_matrix.)
    If a solution doesn't exist, returns np.nan
    """
    a, b, pm = _sanitize_inputs(a, b, pm)
    assert pm.ndim == 2 and pm.shape[1] == 2, "pm is not a valid power matrix."
    assert a.ndim == 2, 'a should be a ndarray matrix'
    assert b.ndim == 1, 'b should be a ndarray vector'
    assert a.shape[0] == a.shape[1], 'a should be a square matrix'
    assert a.shape[1] == b.shape[0], 'shapes of a and b don\'t match'
    return inv(a, pm, other=np.expand_dims(b, 1))


# endregion


# region Polynomial arithmetic
@timings._timed
def polyval(p, x, pm):
    """Evaluate polynomial at given inputs x.

    Assumes that operations are performed in field with power matrix pm.
    (See gen_pow_matrix.)
    """
    p, x, pm = _sanitize_inputs(p, x, pm)
    assert pm.ndim == 2 and pm.shape[1] == 2, "pm is not a valid power matrix."
    assert p.ndim == 1, "polynomials should be represented by vectors"
    assert x.ndim == 1, "coefficients x should be in vector form"
    p, = _sanitize_polynomials(p)
    xp = power(np.expand_dims(x, 1),
               np.expand_dims(np.arange(p.size)[::-1], 0), pm)
    return sum(prod(xp, p, pm), axis=-1)


@timings._timed
def polyprod(p1, p2, pm):
    """Compute the product of polynomial vectors p1 and p2.

    Assumes that operations are performed in field with power matrix pm.
    (See gen_pow_matrix.)
    """
    p1, p2, pm = _sanitize_inputs(p1, p2, pm)
    assert pm.ndim == 2 and pm.shape[1] == 2, "pm is not a valid power matrix."
    assert p1.ndim == 1 and p2.ndim == 1, \
        "polynomials should be represented by vectors"
    p1, p2 = _sanitize_polynomials(p1, p2)
    if p2.size > p1.size:
        p1, p2 = p2, p1
    result = np.zeros(p1.size + p2.size - 1, dtype=elem_type)
    resrows = stricks.as_strided(result, shape=(p2.size, p1.size),
                                 strides=2 * result.strides)
    mulrows = prod(np.expand_dims(p2, 1), p1, pm)
    np.bitwise_xor.at(resrows, ..., mulrows)
    result, = _sanitize_polynomials(result)
    return result


@timings._timed
def polyadd(p1, p2):
    """Compute the sum of polynomial vectors p1 and p2."""
    p1, p2 = _sanitize_inputs(p1, p2)
    assert p1.ndim == 1 and p2.ndim == 1, \
        "polynomials should be represented by vectors"
    p1, p2 = _sanitize_polynomials(p1, p2)
    mp = max(p1.size, p2.size)
    p1 = np.pad(p1, (mp - p1.size, 0), mode='constant')
    p2 = np.pad(p2, (mp - p2.size, 0), mode='constant')
    pp = add(p1, p2)
    pp, = _sanitize_polynomials(pp)
    return pp


@timings._timed
def polydivmod(p1, p2, pm):
    """Compute the quotient and remainder of polynomial vectors p1 and p2.

    Assumes that operations are performed in field with power matrix pm.
    (See gen_pow_matrix.)
    """
    p1, p2 = _sanitize_inputs(p1, p2)
    assert pm.ndim == 2 and pm.shape[1] == 2, "pm is not a valid power matrix."
    assert p1.ndim == 1 and p2.ndim == 1, \
        "polynomials should be represented by vectors"
    p1, p2 = _sanitize_polynomials(p1, p2)
    assert np.any(p2), "division by 0 error."
    q, = _sanitize_polynomials(*_sanitize_inputs(0))
    while np.any(p1) and p1.size >= p2.size:
        t = np.pad(divide(p1[0], p2[0], pm),
                   (0, p1.size - p2.size),
                   mode='constant')
        q = polyadd(q, t)
        p1 = polyadd(p1, polyprod(p2, t, pm))
    return q, p1


# endregion


# region Polynomial algorithms
@timings._timed
def euclid(p1, p2, pm, max_deg=0):
    """Compute the Bézout coefficients for polynomial vectors p1 and p2.

    Assumes that operations are performed in field with power matrix pm.
    (See gen_pow_matrix.)

    Uses the extended Euclidean algorithm to compute the polynomial
    coefficients c1 and c2, such that c1*p1 + c2*p2 = gcd(p1, p2).

    If max_deg > 0, terminates the algorithm, when the degree
    of remainder is max_deg or less.
    """
    p1, p2, pm = _sanitize_inputs(p1, p2, pm)
    assert pm.ndim == 2 and pm.shape[1] == 2, "pm is not a valid power matrix."
    assert p1.ndim == 1 and p2.ndim == 1, \
        "polynomials should be represented by vectors"
    p1, p2 = _sanitize_polynomials(p1, p2)
    c00, c01 = _sanitize_polynomials(*_sanitize_inputs([1], [0]))
    c10, c11 = _sanitize_polynomials(*_sanitize_inputs([0], [1]))
    cond = (lambda p: np.any(p)) if max_deg == -1 else (lambda p: p.size > 1+max_deg)
    while cond(p2):
        q, _ = polydivmod(p1, p2, pm)
        p1, p2 = p2, polyadd(p1, polyprod(q, p2, pm))
        c00, c10 = c10, polyadd(c00, polyprod(q, c10, pm))
        c01, c11 = c11, polyadd(c01, polyprod(q, c11, pm))
    return p2, c10, c11


@timings._timed
def minpoly(x, pm):
    """Compute the minimal polynomial, which has elements of x as it's roots.

    Assumes that operations are performed in field with power matrix pm.
    (See gen_pow_matrix.)

    Returns the minimal polynomial, and all the root conjugates of x.
    """
    x, pm = _sanitize_inputs(x, pm)
    assert pm.ndim == 2 and pm.shape[1] == 2, "pm is not a valid power matrix."
    assert x.ndim == 1, "coefficients x should be in vector form"
    pq, = 1 + msb(pm.shape[0])
    x_con = np.empty((pq,) + x.shape, dtype=elem_type)
    x_con[0] = x
    for xl, xr in zip(x_con[:-1], x_con[1:]):
        xr[...] = prod(xl, xl, pm)
    x_con = np.unique(x_con)
    poly, var_x = _sanitize_polynomials(*_sanitize_inputs([1], [1, 0]))
    for xc in x_con:
        poly = polyprod(poly, polyadd(var_x, xc), pm)
    poly, = _sanitize_polynomials(poly)
    return poly, x_con
# endregion
