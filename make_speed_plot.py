import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import matplotlib.cm as cm
import matplotlib.tri as mtri
# noinspection PyUnresolvedReferences
import mpl_toolkits.mplot3d as plt3d
import numpy as np

import bch

# region Caching
import os
import pickle
import functools


def cached(fn):
    """Caching decorator.

    A function that creates a decorator which will use "cache_speeds"
    for caching the results of the decorated function "fn".
    """

    @functools.wraps(fn)
    def wrapped(*args, **kwargs):
        if os.path.exists("cache_speeds"):
            with open("cache_speeds", 'rb') as cache_handle:
                res = pickle.load(cache_handle)
        else:
            res = fn(*args, **kwargs)
            with open("cache_speeds", 'wb') as cache_handle:
                pickle.dump(res, cache_handle)
        return res

    return wrapped


# endregion


@cached
@np.vectorize
def get_bch_speeds(n, t):
    if 2 * t + 1 > n:
        return np.float(0)
    else:
        return bch.BCH(n, t).speed


# Load speed data
maxQ = 10
maxN = 2 ** maxQ - 1
maxT = (maxN - 1) // 2
Q, T = np.mgrid[:maxQ + 1, 1:maxT + 1]
N = 2 ** Q - 1
R = get_bch_speeds(N, T).astype(np.float)
Q, N, T, R = Q.flatten(), N.flatten(), T.flatten(), R.flatten()

# Clip data and triangulate surface
ok = 2 * T + 1 < N
Q, N, T, R = Q[ok], N[ok], T[ok], R[ok]
tri = mtri.Triangulation(N, T)

# Refine triangulation
ref = mtri.UniformTriRefiner(tri)
interp = mtri.LinearTriInterpolator(tri, R, trifinder=tri.get_trifinder())
tri, triR = ref.refine_field(R, interp, subdiv=5)

# Make speed plot
fig = plt.figure(figsize=(9.6, 7.5))
fig.subplots_adjust(left=0, right=1,
                    bottom=0, top=1,
                    wspace=0, hspace=0)
fig.tight_layout()
ax = fig.add_subplot(111, projection='3d')
ax.set_adjustable('box')
for tval in np.unique(T):
    TT = np.equal(tval, T)
    NT = N[TT]
    RT = R[TT]
    order = np.argsort(NT)
    NT, RT = NT[order], RT[order]
    ax.plot(NT, np.broadcast_to(tval, NT.shape), RT, color='k',
            linewidth=0.1, zorder=100)

surface = ax.plot_trisurf(tri, triR, vmin=0, vmax=1,
                          cmap=cm.coolwarm, edgecolor=None,
                          antialiased=False, shade=True,
                          linewidth=0, alpha=None)
ax.set_zlim(0, 1)
ax.zaxis.set_major_locator(ticker.LinearLocator(10))
ax.zaxis.set_major_formatter(ticker.PercentFormatter(xmax=1, symbol=''))
ax.set_xlabel('N')
ax.set_ylabel('T')
ax.set_zlabel('R, %')
fig.colorbar(surface, shrink=.5, aspect=20)
ax.view_init(azim=145, elev=20)
fig.savefig('images/speed.png', bbox_inches='tight', dpi=200)
