import sys

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np

import bch

# region Caching
import os
import pickle
import functools


def cached(fn, postfix=''):
    """Caching decorator.

    A function that creates a decorator which will use "cache_statistics"
    for caching the results of the decorated function "fn".
    """

    @functools.wraps(fn)
    def wrapped(*args, **kwargs):
        if os.path.exists("cache_statistics" + postfix):
            with open("cache_statistics" + postfix, 'rb') as cache_handle:
                res = pickle.load(cache_handle)
        else:
            res = fn(*args, **kwargs)
            with open("cache_statistics" + postfix, 'wb') as cache_handle:
                pickle.dump(res, cache_handle)
        return res

    return wrapped


# endregion


n = 31
t = int(sys.argv[1])
trials = 100
code = bch.BCH(n, t)
T = np.arange(1 + n)
int_nan = np.empty(code.clen, dtype=np.int_)
int_nan[...] = np.nan


def get_bch_statistics(T):
    print("Running with T={}".format(T))
    messages = np.random.randint(2, size=(trials, code.cmsg))
    messages = code.encode(messages)
    scrambled = messages.copy()
    for msg in scrambled:
        msg[np.random.permutation(code.clen)[:T]] ^= 1
    decoded = code.decode(scrambled)
    right = np.count_nonzero(np.all(decoded == messages, axis=-1))
    abort = np.count_nonzero(np.any(decoded == int_nan, axis=-1))
    wrong = trials - (right + abort)
    return right, wrong, abort


get_bch_statistics = cached(np.vectorize(get_bch_statistics),
                            "_BCH({},{})".format(n, t))
right, wrong, abort = get_bch_statistics(T)
assert (np.all(right + wrong + abort == trials))

mpl.rc('axes', axisbelow=True)
fig, ax = plt.subplots(1, 1, figsize=(9.6, 7.5))
for ind, spine in ax.spines.items():
    if ind != 'top':
        spine.set_visible(False)
ax.set_adjustable('box')
ax.invert_yaxis()

ax.barh(T, left=0, width=right,
        height=.9, color='C2', label='Right')
ax.barh(T, left=right, width=wrong,
        height=.9, color='C3', label='Wrong')
ax.barh(T, left=right + wrong, width=abort,
        height=.9, color='C0', label='Abort')

ax.set_xlim(0, trials)
ax.set_ylim(T.min() - .5, T.max() + .5)

ax.xaxis.set_major_locator(ticker.LinearLocator(10))
ax.xaxis.set_major_formatter(ticker.PercentFormatter(xmax=trials, symbol=''))
ax.xaxis.set_label_position('top')

ax.set_yticks(T)
ax.tick_params(top=True, bottom=False, left=False, right=False,
               labeltop=True, labelbottom=False, labelleft=True)
ax.tick_params(which='minor', bottom=False, top=True)

ax.legend(ncol=3, loc='upper center', bbox_to_anchor=(.5, 0))

ax.set_xlabel('Decoding Results, %')
ax.set_ylabel('Errors Made')
fig.savefig('images/statistics_bch-{}-{}.png'.format(n, t),
            bbox_inches='tight', dpi=200)
