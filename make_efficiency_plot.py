import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import numpy as np

import bch

# region Caching
import os
import pickle
import functools
import timeit


def cached(fn):
    """Caching decorator.

    A function that creates a decorator which will use "cache_efficiencies"
    for caching the results of the decorated function "fn".
    """

    @functools.wraps(fn)
    def wrapped(*args, **kwargs):
        if os.path.exists("cache_efficiencies"):
            with open("cache_efficiencies", 'rb') as cache_handle:
                res = pickle.load(cache_handle)
        else:
            res = fn(*args, **kwargs)
            with open("cache_efficiencies", 'wb') as cache_handle:
                pickle.dump(res, cache_handle)
        return res

    return wrapped


# endregion

n = 127
t = (n - 1) // 2
trials = 4
repeat = 2
code = bch.BCH(n, t)
T = np.arange(1, 1 + t)


def get_bch_efficiencies(T):
    print("Running with T={}".format(T), end=' ')
    messages = np.random.randint(2, size=(trials, code.cmsg))
    messages = code.encode(messages)
    messages[:, np.random.permutation(code.clen)[:T]] ^= 1
    pgz = min(timeit.repeat(lambda: code.decode(messages, method='pgz'),
                            number=1, repeat=repeat))
    euc = min(timeit.repeat(lambda: code.decode(messages, method='euclid'),
                            number=1, repeat=repeat))
    print((pgz, euc))
    return pgz, euc


get_bch_efficiencies = cached(np.vectorize(get_bch_efficiencies))
PGZ, EUC = get_bch_efficiencies(T)

mpl.rc('axes', axisbelow=True)
fig, ax = plt.subplots(1, 1, figsize=(9.6, 7.5))
ax.set_adjustable('box')
c0, c1 = cm.coolwarm([0.0, 1.0])
ax.plot(T, PGZ, color=c0, label='PGZ')
ax.plot(T, EUC, color=c1, label='Euclid')
ax.set_ylim(0, None)
ax.set_xlim(T.min() - 1, T.max() + 1)
ax.set_xticks(T[::4])
ax.set_yticks(np.linspace(0, 0.7, 15))
ax.grid()
ax.set_xlabel('Errors Made')
ax.set_ylabel('Decoding Time')
ax.legend()
fig.savefig('images/efficiency.png', bbox_inches='tight', dpi=200)
