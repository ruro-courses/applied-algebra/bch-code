"""Implementation of Bose–Chaudhuri–Hocquenghem codes."""

import numpy as np
import numpy.lib.stride_tricks as stricks
import itertools

import gf
import timings


class BCH(object):
    """Bose–Chaudhuri–Hocquenghem error-correcting code."""

    def __init__(self, n, t, primpoly_order=0):
        """Create a new encoder-decoder instance.

        Creates a code of length n, that can correct at least t errors.
        If primpoly_order=i is specified, uses ith the smallest primitive
        polynomial as the basis for the underlying Galois Field.
        """
        # Initialize constants
        self.clen = n
        self.nerr = t
        self.cord = 1 + gf.msb(self.clen)
        self.cd = 2 * self.nerr + 1

        # Check if specified BCH code is supported
        assert 2 ** self.cord - 1 == n, \
            "Only primitive BCH codes are supported"
        assert self.cd <= self.clen, \
            "Distance can't be greater than code length"

        # Setup Galois Field
        primitives = gf.get_primitives()
        primitives = list(filter(
            lambda pp: gf.msb(pp) == self.cord, primitives)
        )
        self.primpoly = primitives[primpoly_order % len(primitives)]
        self.pm = gf.gen_pow_matrix(self.primpoly)

        # Zeros of the code, their inverse and the generator polynomial
        self.P = self.pm[:, 1]
        self.R = self.P[:2 * self.nerr]
        self.g, self.Rc = gf.minpoly(self.R, self.pm)

        # Check correctness
        binomial = np.zeros(self.clen + 1, dtype=gf.elem_type)
        binomial[[0, -1]] = 1
        _, rem = gf.polydivmod(binomial, self.g, self.pm)
        assert not np.any(rem), \
            "Generator polynomial g should divide x^n - 1 evenly"
        assert np.all(np.isin(np.unique(self.g), np.asarray([0, 1]))), \
            "Generator polynomial should have binary coefficients"

        self.cerr = self.g.size - 1
        self.cmsg = self.clen - self.cerr
        self.td = None
        self.speed = self.cmsg / self.clen

    @timings._timed
    def encode(self, u):
        """Encode an array of messages u."""
        if u.ndim == 1:
            return self.encode_one(u)[np.newaxis]
        assert u.ndim == 2, "encode method operates on multiple messages"
        assert u.shape[1] == self.cmsg, "message has incorrect length"
        encoded = np.zeros((u.shape[0], self.clen), dtype=gf.elem_type)
        for enc, msg in zip(encoded, u):
            enc[...] = self.encode_one(msg)
        return encoded

    @timings._timed
    def encode_one(self, u):
        """Encode a single message u."""
        assert u.ndim == 1, "encode method operates on multiple messages"
        assert u.shape[0] == self.cmsg, "message has incorrect length"
        xup = np.pad(u, (0, self.cerr), mode='constant')
        _, r = gf.polydivmod(xup, self.g, self.pm)
        v = gf.polyadd(xup, r)
        return np.pad(v, (self.clen - v.size, 0), mode='constant')

    @timings._timed
    def decode(self, w, method=None):
        """Decode an array of messages u using the specified method.

        If method is not specified, uses euclid.
        """
        assert method in [None, 'euclid', 'pgz'], "Invalid decoding method"
        method = method or 'euclid'
        if w.ndim == 1:
            dec = self.decode_one(w, method)
            return np.broadcast_to(dec, w.shape)[np.newaxis]
        assert w.ndim == 2, "decode method operates on multiple messages"
        assert w.shape[1] == self.clen, "received message has incorrect length"
        decoded = np.zeros(w.shape, dtype=gf.elem_type)
        for dec, msg in zip(decoded, w):
            dec[...] = self.decode_one(msg, method=method)
        return decoded

    @timings._timed
    def decode_one(self, w, method):
        """Decode a single message u using the specified method."""
        assert w.ndim == 1, "decode_one method operates on singular messages"
        assert w.shape[0] == self.clen

        syn = gf.polyval(w, self.R, self.pm)
        if not np.any(syn):
            return w
        if method is 'pgz':
            for mu in range(self.nerr, 0, -1):
                loc_mat = stricks.as_strided(syn, shape=(mu, mu + 1),
                                             strides=2 * syn.strides)
                locator = gf.linsolve(loc_mat[:, :-1], loc_mat[:, -1], self.pm)
                if locator is not np.nan:
                    break
            else:
                return np.nan
            locator = gf.polyadd(gf.poly_one,
                                 gf.polyprod(locator, gf.poly_var, self.pm))
        elif method is 'euclid':
            sp = np.pad(syn[::-1], (0, 1), mode='constant')
            xp = np.pad([1], (0, 2 * self.nerr + 1), mode='constant')
            _, locator, _ = gf.euclid(sp, xp, self.pm, max_deg=self.nerr)
        else:
            raise NotImplementedError(
                "Method {} is not implemented.".format(method)
            )
        roots = np.flatnonzero(0 == gf.polyval(locator, self.P, self.pm))
        errors = np.zeros(self.clen, dtype=gf.elem_type)
        errors[roots] = 1
        w_fixed = gf.add(w, errors)
        syn = gf.polyval(w_fixed, self.R, self.pm)
        _, rem = gf.polydivmod(w_fixed, self.g, self.pm)
        if np.any(syn) or np.any(rem):
            return np.nan
        return np.pad(w_fixed, (self.clen - w_fixed.size, 0), mode='constant')

    @timings._timed
    def dist(self):
        """Compute the true distance of this code."""
        if self.td is None:
            msgs = self.cmsg * (np.asarray([0, 1]),)
            msgs = itertools.product(*msgs)
            assert not np.any(next(msgs))
            buffer = np.empty(self.cmsg, dtype=gf.elem_type)

            def hamming(v):
                buffer[...] = v
                x = self.encode_one(buffer)
                assert np.all(np.isin(np.unique(x), [0, 1]))
                return np.count_nonzero(x)

            self.td = min(hamming(word) for word in msgs)
        assert self.td >= self.cd
        return self.td
